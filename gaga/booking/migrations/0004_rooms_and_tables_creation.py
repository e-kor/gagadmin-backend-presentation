from django.db import migrations


def create_rooms_and_tables(apps, schema_editor):
    Room = apps.get_model('booking', 'Room')
    Table = apps.get_model('booking', 'Table')
    for room_data in ROOMS:
        room = Room.objects.create(name=room_data['room_name'])
        tables = [Table(name=table_data['table_name'], max_persons_count=table_data['max_persons_count'], room=room)
                  for table_data in room_data['tables']]

        Table.objects.bulk_create(tables)


class Migration(migrations.Migration):
    dependencies = [('booking', '0003_auto_20200530_1438')]
    operations = [migrations.RunPython(create_rooms_and_tables)]


ROOMS = [
    {
        'room_name': 'Зал',
        'tables': [
            {
                'table_name': 'Диван у сцены',
                'max_persons_count': 10
            },
            {
                'table_name': 'Барная стойка',
                'max_persons_count': 6
            },
            {
                'table_name': 'Подиум',
                'max_persons_count': 6
            },
            {
                'table_name': 'Малый диван',
                'max_persons_count': 8
            },
            {
                'table_name': 'Большой диван',
                'max_persons_count': 8
            },
            {
                'table_name': 'Первый стол окно',
                'max_persons_count': 4
            },
            {
                'table_name': 'Второй стол окно',
                'max_persons_count': 4
            },
            {
                'table_name': 'Первый стол вход',
                'max_persons_count': 3
            },
            {
                'table_name': 'Второй стол вход',
                'max_persons_count': 3
            }
        ]
    },
    {
        'room_name': 'Классная',
        'tables': [
            {
                'table_name': 'Первый большой стол',
                'max_persons_count': 8
            },
            {
                'table_name': 'Второй большой стол',
                'max_persons_count': 8
            },
            {
                'table_name': 'Третий большой стол',
                'max_persons_count': 8
            },
            {
                'table_name': 'Четвертый большой стол',
                'max_persons_count': 8
            },
            {
                'table_name': 'Малый стол',
                'max_persons_count': 2
            },
            {
                'table_name': 'Ps4',
                'max_persons_count': 2
            }
        ]
    },
    {
        'room_name': 'Гостиная',
        'tables': [
            {
                'table_name': 'Первый стол',
                'max_persons_count': 8
            },
            {
                'table_name': 'Второй стол',
                'max_persons_count': 8
            },
            {
                'table_name': 'Третий стол',
                'max_persons_count': 8
            },
            {
                'table_name': 'Четвертый стол',
                'max_persons_count': 8
            },
            {
                'table_name': 'Детский уголок',
                'max_persons_count': 4
            },
            {
                'table_name': 'Диван',
                'max_persons_count': 4
            },
            {
                'table_name': 'Xbox',
                'max_persons_count': 2
            }
        ]
    }
]
