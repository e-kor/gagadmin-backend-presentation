from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from rest_framework.exceptions import ValidationError

from gaga.booking.factories import ReservationFactory
from gaga.booking.models import Reservation, Table

BASE_START = timezone.now().replace(hour=9, minute=0, second=0, microsecond=0)


class BaseReservationNoFinishTestCase(TestCase):
    START = BASE_START
    FINISH = None
    NEXT_DURATION = timedelta(hours=2)

    def setUp(self):
        self.base_reservation = ReservationFactory(start=self.START,
                                                   finish=self.FINISH,
                                                   status=Reservation.BOOKED)

    def create_next_reservation(self, delta, finish=None, tables=None):
        tables = tables or self.base_reservation.tables.all()
        return ReservationFactory(start=self.START + delta, finish=finish,
                                  tables=tables)

    def test_base_reservation_creation(self):
        self.assertEqual(self.base_reservation.start, self.START)
        self.assertEqual(self.base_reservation.finish, self.FINISH)

    def test_next_day(self):
        r = self.create_next_reservation(timedelta(days=1))
        for new_res_table, old_res_table in zip(r.tables.all(),
                                                self.base_reservation.tables.all()):
            self.assertEqual(new_res_table.id, old_res_table.id)

    def test_this_day_after(self):
        self.assertRaises(ValidationError, self.create_next_reservation,
                          timedelta(hours=2))

    def test_this_day_before(self):
        self.assertRaises(ValidationError, self.create_next_reservation,
                          timedelta(hours=-2))

    def test_this_day_before_closed(self):
        delta =  self.NEXT_DURATION
        r = self.create_next_reservation(delta=- delta,
                                         finish=self.START)
        self.assertEqual(r.finish, self.START)
        self.assertEqual(r.start, self.START - delta)

    def test_this_day_before_postpone(self):
        delta =  self.NEXT_DURATION
        r = self.create_next_reservation(delta=- delta,
                                         finish=self.START)
        def postpone(reservation):
            reservation.start += delta
            reservation.finish += delta
            reservation.save()
        self.assertRaises(ValidationError, postpone, r)


    def test_intersect_time_other_tables(self):
        tables = Table.objects.exclude(
            id__in=self.base_reservation.tables.all().values_list("id",
                                                                  flat=True))
        r = self.create_next_reservation(delta=timedelta(), finish=self.FINISH,
                                         tables=tables)
        self.assertEqual(r.start, self.base_reservation.start)
        self.assertEqual(r.finish, self.base_reservation.finish)
# #

class BaseReservationHasFinishTestCase(BaseReservationNoFinishTestCase):
    FINISH = BASE_START + timedelta(hours=6)

    # def test_this_day_after(self):
    #     self.assertRaises(ValidationError, self.create_next_reservation,
    #                       timedelta(hours=2))
    #     r = self.create_next_reservation(self.FINISH - self.START)
    #     self.assertEqual(r.start, self.base_reservation.finish)
