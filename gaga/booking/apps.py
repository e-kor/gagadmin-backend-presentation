from django.apps import AppConfig


class BookingConfig(AppConfig):
    name = "gaga.booking"
    verbose_name = "Booking"

    def ready(self):
        try:
            import gaga.booking.signals  # noqa F401
        except ImportError:
            pass
