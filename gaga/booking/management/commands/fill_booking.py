import logging
from datetime import timedelta
from random import choice, randint

from django.core.management.base import BaseCommand
from django.db import IntegrityError
from faker import Faker

from gaga.booking.factories import ReservationFactory
from gaga.booking.models import Reservation

fake = Faker()
RESERVATIONS_COUNT = 10 ** 3


class Command(BaseCommand):

    def handle(self, *args, **options):
        success_count = 0
        for _ in range(RESERVATIONS_COUNT):
            try:
                start = fake.date_time_this_year()
                duration = timedelta(hours=randint(1, 6))
                finish = choice([None, start + duration])
                ReservationFactory(start=start, finish=finish)
                success_count += 1
            except IntegrityError:
                pass
        logging.info("Created %s reservations. Total count - %s", success_count,
                     Reservation.objects.count())
