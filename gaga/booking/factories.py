from datetime import timedelta
from random import choice, randint

from factory import DjangoModelFactory, Faker, fuzzy, post_generation

from gaga.booking.models import Reservation, Table

MAX_TABLES = 5


class ReservationFactory(DjangoModelFactory):
    contact_name = Faker("name")
    contact_phone = Faker("phone_number")
    notes = Faker('text', max_nb_chars=300, ext_word_list=None)
    status = fuzzy.FuzzyChoice(Reservation.STATUS_CHOICES,
                               getter=lambda c: c[0])
    start = Faker("date_time_this_year")
    finish = Faker("date_time_this_year")

    is_periodic = Faker("pybool")

    class Meta:
        model = Reservation

    @post_generation
    def tables(self, create: bool, extracted, **kwargs):
        if extracted:
            self.tables.set(extracted)
        else:
            self.tables.set([choice(Table.objects.all()) for _ in
                             range(randint(1, MAX_TABLES))])

