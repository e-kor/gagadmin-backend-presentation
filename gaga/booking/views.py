from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import DateField
from django.db.models.functions import Cast
from django_filters.rest_framework import DateFilter, \
    DjangoFilterBackend, FilterSet, NumberFilter
from rest_framework import viewsets
from rest_framework.filters import SearchFilter

from gaga.booking.models import Reservation, Room, Table
from gaga.booking.serializers import ReservationSerializer, RoomSerializer, \
    TableSerializer
from gaga.permissions import IsAdmin, IsSuperUser, ReadOnly


class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = [IsSuperUser | ReadOnly]


class TableFilter(FilterSet):
    start = DateFilter(field_name="reservations__start",
                       method="filter_reservations")
    max_persons_count = NumberFilter(field_name="max_persons_count",
                                     lookup_expr='exact')

    def filter_reservations(self, queryset, name, value):
        return (queryset.filter(reservations__status=Reservation.BOOKED)
                .annotate(starts=ArrayAgg(Cast('reservations__start',
                                               DateField())))
                .exclude(starts__contains=[value]))

    class Meta:
        model = Table
        fields = ["start", "max_persons_count", ]


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = TableFilter
    permission_classes = [IsSuperUser | ReadOnly]


class ReservationFilter(FilterSet):
    class Meta:
        model = Reservation
        fields = {
            'is_periodic': ["exact"],
            "status": ["exact"],
            "start": ["lte", "gte"],
            "finish": ["lte", "gte"],
        }


class ReservationViewSet(viewsets.ModelViewSet):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    search_fields = ["contact_name", "contact_phone", "notes"]
    filterset_class = ReservationFilter
    permission_classes = [IsSuperUser | IsAdmin | ReadOnly]
