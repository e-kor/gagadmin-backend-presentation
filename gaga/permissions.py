from rest_framework.permissions import BasePermission, OR, OperandHolder, \
    SAFE_METHODS

from gaga.users.models import User


class IsSuperUser(BasePermission):
    def has_permission(self, request, view):
        return getattr(request.user, "role", None) == User.SUPERUSER


class IsStuff(BasePermission):
    def has_permission(self, request, view):
        return getattr(request.user, "role", None) == User.STUFF


class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        return getattr(request.user, "role", None) == User.ADMIN


class IsOwner(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user


IsOwnerOrSuperUser = OperandHolder(OR, IsSuperUser, IsOwner)


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS
