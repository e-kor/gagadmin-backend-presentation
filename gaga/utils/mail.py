from django.core.mail import EmailMessage, BadHeaderError
from django.template.loader import render_to_string
from django.urls import reverse
from django_rest_passwordreset.models import ResetPasswordToken
from django.conf import settings


def send_invite_msg(token: ResetPasswordToken):
    """
    Sends invite message with reset token to user by email
    """
    context = {
        'set_password_url': '{domain}/{path}?token={token}'.format(
            domain=settings.DOMAIN, path='users/set-password', token=token.key)
    }
    email_html_msg = render_to_string('email_set_password.html', context)

    subject = getattr(settings, 'EMAIL_SUBJECT_PREFIX', '[Welcome]')
    from_email = getattr(settings, 'DEFAULT_FROM_EMAIL', '')
    to = [token.user.email]
    email = EmailMessage(subject=subject, body=email_html_msg, from_email=from_email, to=to)
    email.content_subtype = 'html'
    email.send()
