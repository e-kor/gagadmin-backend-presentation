from django.apps import AppConfig


class BillingConfig(AppConfig):
    name = "gaga.billing"
    verbose_name = "Billing"

    def ready(self):
        try:
            import gaga.billing.signals  # noqa F401
        except ImportError:
            pass
