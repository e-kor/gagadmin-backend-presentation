from factory import DjangoModelFactory, Faker, RelatedFactory

from gaga.billing.models import Guest, Visit


class GuestFactory(DjangoModelFactory):
    card_number = Faker("user_name")

    class Meta:
        model = Guest


class VisitFactory(DjangoModelFactory):
    guest = RelatedFactory(GuestFactory)
    bonus_receiver = RelatedFactory(GuestFactory)

    class Meta:
        model = Visit
