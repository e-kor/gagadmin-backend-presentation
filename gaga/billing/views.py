from datetime import timedelta

from django.utils import timezone
from django_filters import FilterSet
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.request import Request
from rest_framework.response import Response

from gaga.billing.models import Constant, FixedRate, TimeRate, Visit
from gaga.billing.serializers import ConstantSerializer, FixedRateSerializer, \
    TimeRateSerializer, VisitSerializer
from gaga.permissions import IsAdmin, IsSuperUser, ReadOnly


class FixedRateViewSet(viewsets.ModelViewSet):
    queryset = FixedRate.objects.all()
    serializer_class = FixedRateSerializer
    pagination_class = None
    permission_classes = [IsSuperUser | IsAdmin | ReadOnly]


class TimeRateViewSet(viewsets.ModelViewSet):
    queryset = TimeRate.objects.all()
    serializer_class = TimeRateSerializer
    pagination_class = None
    permission_classes = [IsSuperUser | IsAdmin | ReadOnly]

    @swagger_auto_schema(responses={status.HTTP_200_OK: ConstantSerializer})
    @action(detail=False, methods=["get"], url_path="total-max")
    def total_max(self, request):
        return Response({"value": TimeRate.total_max})

    @swagger_auto_schema(request_body=ConstantSerializer,
                         responses={status.HTTP_200_OK: ConstantSerializer})
    @total_max.mapping.put
    def update_total_max(self, request: Request):
        new_max = request.data.get("value")
        if not new_max:
            raise ValidationError({"value": "this field shouldn't be empty"})
        max_, _ = Constant.objects.get_or_create(name="TOTAL_MAX")
        max_.value = new_max
        max_.save()
        return Response({"value": TimeRate.total_max})


class VisitFilter(FilterSet):
    class Meta:
        model = Visit
        fields = {
            'guest': ["exact"],
            "start": ["lte", "gte"],
            "finish": ["lte", "gte"],
        }


class VisitViewSet(viewsets.ModelViewSet):
    queryset = Visit.objects.all().select_related("bill")
    serializer_class = VisitSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = VisitFilter
    permission_classes = [IsSuperUser | IsAdmin | ReadOnly]

    @action(detail=False, methods=['get'])
    def today(self, request):
        start_of_day = timezone.now().replace(hour=8, minute=0, second=0)
        finish_of_day = timezone.now().replace(hour=5, minute=0,
                                               second=0) + timedelta(days=1)
        visits = Visit.objects.filter(start__gte=start_of_day,
                                      start__lte=finish_of_day)
        return Response(VisitSerializer(visits, many=True).data)
