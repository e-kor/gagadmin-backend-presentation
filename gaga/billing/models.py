from datetime import datetime, timedelta
from typing import Tuple

from django.contrib.postgres.fields import ArrayField, JSONField
from django.db import models
from django.utils import timezone
from django.utils.decorators import classproperty


class Constant(models.Model):
    name = models.CharField(max_length=40)
    value = models.DecimalField(max_digits=10, decimal_places=2)


class Guest(models.Model):
    card_number = models.CharField(max_length=100)
    bonus = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return self.card_number


class FixedRate(models.Model):
    name = models.CharField(max_length=300)
    cost = models.DecimalField(max_digits=10, decimal_places=2)


class TimeRate(models.Model):
    cost = models.DecimalField(max_digits=10, decimal_places=2)
    start_hour = models.PositiveSmallIntegerField()
    finish_hour = models.PositiveSmallIntegerField()
    days_of_week = ArrayField(models.PositiveSmallIntegerField(), size=7)
    last_update = models.DateTimeField(auto_now_add=True)

    DEFAULT_TOTAL_MAX = 650
    DEFAULT_DEFAULT_PER_HOUR_COST = 2

    @classproperty
    def default_per_hour_cost(self):
        constant = Constant.objects.filter(name="DEFAULT_PER_HOUR_COST").first()
        return getattr(constant, "value", self.DEFAULT_DEFAULT_PER_HOUR_COST)

    @classproperty
    def total_max(self):
        constant = Constant.objects.filter(name="TOTAL_MAX").first()
        return getattr(constant, "value", self.DEFAULT_TOTAL_MAX)

    @classmethod
    def calculate_visit_cost(cls, start: datetime, finish: datetime
                             ) -> Tuple[float, int]:
        total = 0
        while start < finish:
            hour = start.hour
            end_of_hour = start.replace(minute=0, second=0) + timedelta(hours=1)

            rate = TimeRate.objects.filter(start_hour__lte=hour,
                                           finish_hour__gt=hour,
                                           days_of_week__contains=[
                                               start.weekday() + 1]).last()
            rate_cost = getattr(rate, "cost", cls.default_per_hour_cost)
            delta = min(end_of_hour, finish) - start
            total += delta.total_seconds() / 60 * float(rate_cost)
            start = end_of_hour
        total = min(total, cls.total_max)
        return total, int((finish - start).total_seconds() / 60)


class Visit(models.Model):
    guest = models.ForeignKey(Guest, related_name="visits",
                              on_delete=models.CASCADE)
    bonus_receiver = models.ForeignKey(Guest, related_name="bonus_visits",
                                       on_delete=models.CASCADE)
    start = models.DateTimeField(default=timezone.now)
    finish = models.DateTimeField(null=True)
    fixed_rate = models.ForeignKey(FixedRate, on_delete=models.SET_NULL,
                                   null=True)

    def __str__(self):
        return f"{self.start}-P{self.finish} [{self.guest}]"


class Bill(models.Model):
    total = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    visit = models.OneToOneField(Visit, on_delete=models.CASCADE)
    details = JSONField(default=dict)
