from django.db.models.signals import post_save, pre_save
from rest_framework.exceptions import ValidationError

from gaga.billing.models import Bill, TimeRate, Visit
from gaga.billing.serializers import FixedRateSerializer


def add_default_bonus_receiver(sender, instance: Visit, *args, **kwargs):
    if not hasattr(instance, "bonus_receiver") and hasattr(instance, "guest"):
        instance.bonus_receiver = instance.guest


pre_save.connect(add_default_bonus_receiver, sender=Visit)


def check_start_finish(sender, instance: Visit, *args, **kwargs):
    if hasattr(instance, "finish") and instance.finish:
        if instance.start >= instance.finish:
            raise ValidationError({"finish": "should be less than start"})


pre_save.connect(check_start_finish, sender=Visit)


def calculate_bill(sender, instance: Visit, *args, **kwargs):
    if instance.fixed_rate:
        bill, _ = Bill.objects.get_or_create(visit=instance)
        bill.total = instance.fixed_rate.cost
        bill.details = {
            "rate_type": "fixed",
            "details": FixedRateSerializer(instance.fixed_rate).data
        }
        bill.save(update_fields=["total", "details"])
    else:
        if instance.finish:
            bill, _ = Bill.objects.get_or_create(visit=instance)
            bill.total, minutes_count = TimeRate.calculate_visit_cost(
                instance.start,
                instance.finish)
            bill.details = {
                "rate_type": "per_minute",
                "details": {
                    "start": instance.start.isoformat(),
                    "finish": instance.finish.isoformat(),
                    "minutes_count": minutes_count}
            }
            bill.save(update_fields=["total", "details"])
        else:
            instance.bill = None
            instance.save()


post_save.connect(calculate_bill, sender=Visit)
