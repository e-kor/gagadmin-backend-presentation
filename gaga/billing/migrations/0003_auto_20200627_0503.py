# Generated by Django 3.0.5 on 2020-06-27 02:03

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0002_init_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='bill',
            name='details',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='visit',
            name='bonus_receiver',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='bonus_visits', to='billing.Guest'),
        ),
    ]
