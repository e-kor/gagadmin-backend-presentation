from django.db import migrations


def alter_time_rate(apps, schema_editor):
    TimeRate = apps.get_model('billing', 'TimeRate')
    r = TimeRate.objects.get(days_of_week__contains=[1, 2, 3, 4, 5, 6, 7])
    r.days_of_week = [6, 7]
    r.save()


class Migration(migrations.Migration):
    dependencies = [('billing', '0004_auto_20200627_1352')]
    operations = [
        migrations.RunPython(alter_time_rate),

    ]
