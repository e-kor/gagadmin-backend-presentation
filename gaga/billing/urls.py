from rest_framework.routers import SimpleRouter

from gaga.billing.views import FixedRateViewSet, TimeRateViewSet, VisitViewSet

router = SimpleRouter()
router.register("fixed-rates", FixedRateViewSet)
router.register("time-rates", TimeRateViewSet)
router.register("visits", VisitViewSet)
urlpatterns = router.urls
