from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = "gaga.users"
    verbose_name = "Users"

    def ready(self):
        try:
            import gaga.users.signals  # noqa F401
        except ImportError:
            pass
