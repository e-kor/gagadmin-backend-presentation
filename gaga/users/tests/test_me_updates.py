from copy import copy

from rest_framework import status

from gaga.users.models import User
from gaga.users.tests.base import  BASE_URL, USER_INFO
from gaga.base_tests import AuthenticatedAPITestCase

URL = f"{BASE_URL}me/"
REQUIRED_FIELDS = ("username", "email")
FROZEN_FIELDS = ["username", "role"]


class MeUpdateTest(AuthenticatedAPITestCase):
    def setUp(self):
        super().setUp()
        self.old_values = dict(zip(FROZEN_FIELDS, [getattr(self.user, attr)
                                                   for attr in FROZEN_FIELDS]))

    def test_me_update(self):
        response = self.client.put(URL, USER_INFO, format='json')
        self.user.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user_info = copy(USER_INFO)
        user_data = user_info.pop("data")
        for key, value in user_info.items():
            if key in FROZEN_FIELDS:
                self.assertEqual(self.old_values[key], getattr(self.user, key))
                self.assertEqual(self.old_values[key], response.data[key])
            else:
                self.assertEqual(value, getattr(self.user, key))
                self.assertEqual(value, response.data[key])
        for key, value in user_data.items():
            self.assertEqual(value, getattr(self.user.data, key))
            self.assertEqual(value, response.data["data"][key])

    def test_required_field(self):
        for required_field in REQUIRED_FIELDS:
            user_info = copy(USER_INFO)
            user_info.pop(required_field)
            response = self.client.put(URL, user_info, format="json")
            self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)


class TestMeUpdateForSuperuser(MeUpdateTest):
    ROLE = User.SUPERUSER


class TestMeUpdateForAdmin(TestMeUpdateForSuperuser):
    ROLE = User.ADMIN


class TestUserCreationForStuff(TestMeUpdateForAdmin):
    ROLE = User.STUFF
