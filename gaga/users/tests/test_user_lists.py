from rest_framework import status

from gaga.users.models import User
from gaga.users.tests.base import BASE_URL
from gaga.users.factories import UserFactory
from gaga.base_tests import AuthenticatedAPITestCase

USERS_COUNT = 10
URL = BASE_URL


class UserListTest(AuthenticatedAPITestCase):

    @classmethod
    def setUpTestData(cls):
        [UserFactory() for _ in range(USERS_COUNT)]

    def test_get_user_list(self):
        response = self.client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        return response


class TestUserListForAdmin(UserListTest):
    ROLE = User.ADMIN

    def test_get_user_list(self):
        response = super().test_get_user_list()
        self.assertEqual(User.objects.filter(is_active=True).count(),
                         len(response.data))


class TestUserListForStuff(TestUserListForAdmin):
    ROLE = User.STUFF


class TestUserListForSuperuser(UserListTest):
    ROLE = User.SUPERUSER

    def test_get_user_list(self):
        response = super().test_get_user_list()
        self.assertEqual(User.objects.count(), len(response.data))
