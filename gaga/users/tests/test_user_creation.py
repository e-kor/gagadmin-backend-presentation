from copy import copy

from rest_framework import status

from gaga.users.models import User
from gaga.users.tests.base import BASE_URL, \
    REQUIRED_FIELDS, USER_INFO
from gaga.base_tests import AuthenticatedAPITestCase

URL = BASE_URL


class UserCreationTest(AuthenticatedAPITestCase):

    def test_create_user(self):
        response = self.client.post(URL, USER_INFO, format='json')
        return response


class TestUserCreationForSuperuser(UserCreationTest):
    ROLE = User.SUPERUSER

    def test_create_user(self):
        response = super().test_create_user()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        test_user = User.objects.get(username=USER_INFO["username"])
        user_info = copy(USER_INFO)
        user_data = user_info.pop("data")
        for key, value in user_info.items():
            self.assertEqual(value, response.data[key])
            self.assertEqual(value, getattr(test_user, key))
        for key, value in user_data.items():
            self.assertEqual(value, response.data["data"][key])
            self.assertEqual(value, getattr(test_user.data, key))

    def test_required_field(self):
        for required_field in REQUIRED_FIELDS:
            data = copy(USER_INFO).pop(required_field)
            response = self.client.post(URL, data, format="json")
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestUserCreationForAdmin(UserCreationTest):
    ROLE = User.ADMIN

    def test_create_user(self):
        response = super().test_create_user()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TestUserCreationForStuff(TestUserCreationForAdmin):
    ROLE = User.STUFF
