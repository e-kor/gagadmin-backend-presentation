from gaga.users.models import User

BASE_URL = "/api/users/"
USER_INFO = {
    "username": "test_username",
    "role": User.STUFF,
    "data": {
        "shortcuts": ["string"],
        "display_name": "string",
        "position": "string",
        "photo_url": "https://www.google.com/googlelogo_color_272x92dp.png",
        "phone_number": "string",
        "settings": {},
        "custom_scrollbars": True,
        "theme": {}
    },
    "is_active": True,
    "email": "test_username@example.com"
}
REQUIRED_FIELDS = ("username", "email")
