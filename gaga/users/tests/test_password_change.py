from django_rest_passwordreset.models import ResetPasswordToken
from rest_framework import status

from gaga.users.models import User
from gaga.users.tests.base import  BASE_URL
from gaga.base_tests import AuthenticatedAPITestCase

URL = f"{BASE_URL}auth/set-password/"
NEW_PASSWORD = "szlkfsdzt4alij"


class TestPasswordChangeForSuperuser(AuthenticatedAPITestCase):
    ROLE = User.SUPERUSER

    def test_update_password(self):
        token = ResetPasswordToken.objects.create(user=self.user)
        response = self.client.post(URL,
                                    dict(token=str(token.key),
                                         password=NEW_PASSWORD),
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password(NEW_PASSWORD))


class TestPasswordChangeForAdmin(TestPasswordChangeForSuperuser):
    ROLE = User.ADMIN


class TestPasswordChangenForStuff(TestPasswordChangeForAdmin):
    ROLE = User.STUFF
