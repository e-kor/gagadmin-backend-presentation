from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField, JSONField
from django.db import models


class UserData(models.Model):
    display_name = models.CharField(max_length=256)
    position = models.CharField(max_length=256)
    photo_url = models.URLField(null=True, blank=True)
    phone_number = models.CharField(max_length=30, null=True, blank=True)
    settings = JSONField(default=dict)
    custom_scrollbars = models.BooleanField(default=False)
    theme = JSONField(default=dict)
    shortcuts = ArrayField(models.CharField(max_length=20), default=list)


class User(AbstractUser):
    ADMIN = "admin"
    STUFF = "stuff"
    SUPERUSER = "superuser"
    ROLE_CHOICES = [
        (ADMIN, "Administrator"),
        (STUFF, "Stuff"),
        (SUPERUSER, "SuperUser")
    ]
    role = models.CharField("Role of employee", max_length=9,
                            choices=ROLE_CHOICES, default=STUFF)
    data = models.OneToOneField(UserData, on_delete=models.CASCADE, null=True)
    email = models.EmailField(unique=True, blank=False)

