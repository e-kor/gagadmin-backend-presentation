from django_rest_passwordreset.models import ResetPasswordToken
from django_rest_passwordreset.signals import reset_password_token_created
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers, status
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, ListModelMixin, \
    RetrieveModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework_simplejwt.views import (
    TokenObtainPairView)

from gaga.users.models import User
from gaga.permissions import IsSuperUser, ReadOnly
from .serializers import UserSerializer


class UserViewSet(RetrieveModelMixin, ListModelMixin, UpdateModelMixin,
                  CreateModelMixin, GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all().select_related("data")
    lookup_field = "username"
    permission_classes = [IsSuperUser | ReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = self.request.user
        queryset = super().get_queryset()
        if user.role != User.SUPERUSER:
            queryset = queryset.filter(is_active=True)
        return queryset

    @action(detail=False, methods=["PUT"], permission_classes=[IsAuthenticated])
    def me(self, request):
        filtered_data = request.data
        filtered_data.pop("role")
        serializer = UserSerializer(request.user, data=filtered_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    @me.mapping.get
    def update_me(self, request):
        serializer = UserSerializer(request.user, context={"request": request})
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    def create(self, request, *args, **kwargs):
        create_response = super(UserViewSet, self).create(request, *args,
                                                          **kwargs)
        user = User.objects.get(username=create_response.data['username'])
        token = ResetPasswordToken.objects.create(user=user)
        reset_password_token_created.send(sender=self.__class__, instance=self,
                                          reset_password_token=token)
        return create_response


class TokenObtainPairResponseSerializer(serializers.Serializer):
    access = serializers.CharField()
    refresh = serializers.CharField()

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenObtainPairView(TokenObtainPairView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenObtainPairResponseSerializer})
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)
