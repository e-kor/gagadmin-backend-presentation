from typing import Any, Sequence

from factory import DjangoModelFactory, Faker, RelatedFactory, post_generation

from gaga.users.models import User, UserData


class UserDataFactory(DjangoModelFactory):
    display_name = Faker("name")
    position = Faker("job")
    photo_url = Faker("image_url")
    phone_number = Faker("phone_number")
    settings = dict()
    custom_scrollbars = Faker("pybool")
    theme = dict()
    shortcuts = list()

    class Meta:
        model = UserData


class UserFactory(DjangoModelFactory):
    username = Faker("user_name")
    email = Faker("email")
    data = RelatedFactory(UserDataFactory)
    is_active = Faker("pybool")

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs):
        password = (extracted if extracted else Faker("password",
                                                      length=42,
                                                      special_chars=True,
                                                      digits=True,
                                                      upper_case=True,
                                                      lower_case=True,
                                                      ).generate(
            extra_kwargs={}))
        self.set_password(password)

    @post_generation
    def role(self, create: bool, extracted: Sequence[Any], **kwargs):
        self.role = extracted or User.ADMIN

    class Meta:
        model = User
