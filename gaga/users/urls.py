from django.urls import path
from django_rest_passwordreset.views import reset_password_confirm
from rest_framework.routers import SimpleRouter

from gaga.users.views import UserViewSet,  DecoratedTokenObtainPairView

router = SimpleRouter()
router.register("", UserViewSet)
urlpatterns = router.urls

urlpatterns += [
    path('auth/token-obtain/', DecoratedTokenObtainPairView.as_view()),
    path('auth/set-password/', reset_password_confirm),
]
