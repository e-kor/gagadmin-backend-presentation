# Generated by Django 3.0.5 on 2020-05-25 14:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20200525_1739'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userdata',
            old_name='photo_URL',
            new_name='photoURL',
        ),
    ]
