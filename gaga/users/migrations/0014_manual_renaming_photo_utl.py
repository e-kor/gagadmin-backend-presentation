from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('users', '0013_manual_renaming_photoURL'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userdata',
            old_name='photo_utl',
            new_name='photo_url',
        ),
    ]
