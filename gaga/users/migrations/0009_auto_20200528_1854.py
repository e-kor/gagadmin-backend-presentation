from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_auto_20200525_1759'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdata',
            name='phone_number',
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='email',
            field=models.EmailField(default='default@email.com', max_length=254),
            preserve_default=False,
        ),
    ]
