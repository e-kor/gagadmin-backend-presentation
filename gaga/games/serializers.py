from django_elasticsearch_dsl_drf.serializers import DocumentSerializer
from rest_framework import serializers

from gaga.games.documents import GameDocument
from gaga.games.models import Game


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = "__all__"


class GameDocumentSerializer(DocumentSerializer):
    class Meta:
        fields = ["id",
                  "title",
                  "alias",
                  "bgg_geek_rating",
                  "bgg_id",
                  "bgg_num_votes",
                  "bgg_rating",
                  "comments_total",
                  "comments_total_new",
                  "creation_date_utc",
                  "description",
                  "description_short",
                  "modification_date_utc",
                  "n_10rating",
                  "n_20rating",
                  "num_votes",
                  "photo_url",
                  "players_age_min",
                  "players_max",
                  "players_max_recommend",
                  "players_min",
                  "players_min_recommend",
                  "playtime_max",
                  "playtime_min",
                  "rating_user",
                  "tesera_id",
                  "time_to_learn",
                  "year"]
        document = GameDocument
