from django.urls import path
from rest_framework.routers import SimpleRouter

from gaga.games.views import AutocompleteGameDocumentView, GameDocumentView, \
    GamesViewSet

router = SimpleRouter()

router.register("", GameDocumentView, "")
router.register("edit", GamesViewSet)

urlpatterns = [path("suggest/", AutocompleteGameDocumentView.as_view(
    {"get": "suggest"}), ), ] + router.urls
