import logging
from itertools import count

import requests
from django.core.management.base import BaseCommand
from django.db import IntegrityError
from humps import decamelize

from gaga.games.models import Game

DEFAULT_LIMIT = 100
BASE_URL = "https://api.tesera.ru/games"


def get_tesera_partially(limit: int, offset: int) -> int:
    url = f"{BASE_URL}?offset={offset}&limit={limit}&sort=-creationdateutc"
    response = requests.get(url)
    response.raise_for_status()
    logging.info("got %s games from %s [%s]", limit, url, response.status_code)
    games = []
    for games_data in decamelize(response.json()):
        # it more safe to create game one by one instead of bulk create
        try:
            games.append(Game.objects.create(**games_data))
        except IntegrityError:
            logging.info("couldn't insert to db %s: duplicates found",
                         games_data, exc_info=True)
        except Exception:
            logging.error("unknown error %s", games_data, exc_info=True)
    return len(games)


def get_tesera_games(limit: int):
    total_games_count = 0
    for offset in count(0, 1):
        games_count = get_tesera_partially(limit, offset)
        total_games_count += games_count
        logging.info("inserted %s games. %s total", games_count,
                     total_games_count)
        if games_count == 0:
            logging.info("finished getting games from tesera")
            return None


class Command(BaseCommand):
    help = 'downloads all games from tesera api'

    def handle(self, *args, **options):
        get_tesera_games(DEFAULT_LIMIT)
