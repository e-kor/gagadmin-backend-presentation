from django_elasticsearch_dsl_drf.filter_backends import (
    DefaultOrderingFilterBackend, FilteringFilterBackend, OrderingFilterBackend,
    SearchFilterBackend, SourceBackend, SuggesterFilterBackend)
from django_elasticsearch_dsl_drf.pagination import LimitOffsetPagination
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet, \
    SuggestMixin
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from gaga.games.documents import GameDocument
from gaga.games.models import Game
from gaga.games.serializers import GameDocumentSerializer, GameSerializer
from gaga.permissions import IsStuff, IsSuperUser, ReadOnly


class GamesViewSet(mixins.CreateModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.DestroyModelMixin,
                   GenericViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    lookup_field = "id"
    permission_classes = [IsSuperUser | IsStuff | ReadOnly]


class GameDocumentView(BaseDocumentViewSet):
    """The PublisherDocument view."""

    document = GameDocument
    pagination_class = LimitOffsetPagination
    serializer_class = GameDocumentSerializer
    lookup_field = 'id'
    filter_backends = [
        FilteringFilterBackend,
        OrderingFilterBackend,
        DefaultOrderingFilterBackend,
        SearchFilterBackend,

    ]

    search_fields = (
        "alias",
        "title.raw", "title2", "title3"
    )
    filter_fields = {
        "players_age_min": None,
        "players_max": None,
        "players_max_recommend": None,
        "players_min": None,
        "players_min_recommend": None,
        "playtime_max": None,
        "playtime_min": None,
        "time_to_learn": None,
    }
    ordering_fields = {
        "players_age_min": None,
        'bgg_geek_rating': None,
        'bgg_rating': None,
        'rating_user': None,
        'n_10rating': None,
        'n_20rating': None,
        'year': None,
        'id': None,
    }
    ordering = ("year",)
    source = False


class AutocompleteGameDocumentView(SuggestMixin, GameDocumentView):
    """
    extra view made specially for autocomplete
    because if source = True => then the whole objects is in _source field in
    the response. if source = False => it breaks filtering, searching actions

    couldn't find cleaner solution.
    """
    filter_backends = [
        SourceBackend,
        SuggesterFilterBackend
    ]
    suggester_fields = {
        'title': {
            'field': 'title.suggest',
            'options': {
                'size': 20,
                'skip_duplicates': True,
            },

        }}
    source = False
