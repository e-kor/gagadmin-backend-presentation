from django.apps import AppConfig


class GamesConfig(AppConfig):
    name = 'gaga.games'

    def ready(self):
        try:
            import gaga.games.signals  # noqa F401
        except ImportError:
            pass

