from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry

from .models import Game


@registry.register_document
class GameDocument(Document):
    title = fields.TextField(
        fields={
            'raw': fields.TextField(analyzer='keyword'),
            'suggest': fields.CompletionField(),
        }
    )
    id = fields.IntegerField(attr='id')

    class Index:
        name = 'games'
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = Game

        fields = [
            "alias",
            "bgg_geek_rating",
            "bgg_id",
            "bgg_num_votes",
            "bgg_rating",
            "comments_total",
            "comments_total_new",
            "creation_date_utc",
            "description",
            "description_short",
            "modification_date_utc",
            "n_10rating",
            "n_20rating",
            "num_votes",
            "photo_url",
            "players_age_min",
            "players_max",
            "players_max_recommend",
            "players_min",
            "players_min_recommend",
            "playtime_max",
            "playtime_min",
            "rating_user",
            "tesera_id",
            "time_to_learn",
            "year",
        "title2", "title3"]
