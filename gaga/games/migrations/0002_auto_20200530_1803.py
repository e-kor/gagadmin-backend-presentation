# Generated by Django 3.0.5 on 2020-05-30 15:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='game',
            old_name='n10_rating',
            new_name='n_10rating',
        ),
        migrations.RenameField(
            model_name='game',
            old_name='n20_rating',
            new_name='n_20rating',
        ),
    ]
